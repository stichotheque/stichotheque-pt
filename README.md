# Stichotheque

The **Stichotheque** project aims to collect poetry works in order to establish
a multilingual corpus that can be analyzed by computer tools.

This repository contains poems in **Portuguese**.

We do not have a formal license yet, but if you would like to use the files
provided by Stichotheque, these are the informal terms:

- You can use the files for research and education, but not for commercial purposes.
- If you write an article that uses these files in some way, we ask you to cite
  one of [our publications](PUBLICATIONS.md).

The table below lists the works currently available in the Portuguese corpus.
The works whose code is followed by an asterisk are not in the public domain and
therefore are not found in this repository.

| Code     | Poet                                     | Title                                              |       Verses |
| :------- | :--------------------------------------- | :------------------------------------------------- | -----------: |
| `anj01`  | Augusto dos Anjos                        | Obra Completa                                      |        6.595 |
| `boc01`  | Bocage                                   | Poesias Eróticas                                   |        3.477 |
| `cam01`  | Luís de Camões                           | Os Lusíadas                                        |        8.816 |
| `col01*` | Carlos Alberto de Oliveira Leite         | Caxias                                             |        3.880 |
| `cor01*` | Leôncio Correia                          | Brasilíada                                         |        1.874 |
| `cos01`  | Cláudio Manuel da Costa                  | Obras Poéticas                                     |        7.682 |
| `cos02`  | Cláudio Manuel da Costa                  | Vila Rica                                          |        2.718 |
| `cru01`  | Cruz e Sousa                             | Broquéis                                           |          914 |
| `cru02`  | Cruz e Sousa                             | O Livro Derradeiro                                 |        8.243 |
| `dia01`  | Gonçalves Dias                           | Os Timbiras                                        |        2.032 |
| `dia02`  | Gonçalves Dias                           | I-Juca Pirama                                      |          463 |
| `dur01`  | Santa Rita Durão                         | Caramuru                                           |        6.672 |
| `emm01`  | Emílio de Meneses                        | Mortalhas                                          |        1.276 |
| `fag01`  | Fagundes Varela                          | Anchieta                                           |        8.502 |
| `fig01`  | Araújo Figueredo                         | Praias                                             |        2.800 |
| `fig02`  | Araújo Figueredo                         | Novenas de Maio                                    |        2.472 |
| `fig03`  | Araújo Figueredo                         | Filhos e Netos                                     |          490 |
| `fig04`  | Araújo Figueredo                         | Ascetério                                          |        1.225 |
| `fig05`  | Araújo Figueredo                         | Versos Antigos                                     |        1.246 |
| `gam01`  | Basílio da Gama                          | O Uraguai                                          |        1.377 |
| `gam02`  | Basílio da Gama                          | A Declamação Trágica                               |          238 |
| `gon01`  | Tomás Antônio Gonzaga                    | Cartas Chilenas                                    |        4.184 |
| `gon02`  | Tomás Antônio Gonzaga                    | Marília de Dircou                                  |        4.720 |
| `gut01`  | Gustavo Teixeira                         | Ementário                                          |        2.189 |
| `gut02`  | Gustavo Teixeira                         | Poemas Líricos                                     |          990 |
| `jjc01`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume I      |        1.856 |
| `jjc02`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume II     |        2.936 |
| `jjc03`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume III    |        2.745 |
| `jjc04`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume IV     |        2.658 |
| `jjc05`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume V      |        1.914 |
| `jjc06`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume VI     |        1.945 |
| `jjc07`  | Padre Correia de Almeida                 | Sátiras, Epigramas e Outras Poesias: Volume VII    |        1.971 |
| `lim01*` | Jorge de Lima                            | Permanência de Inês                                |          143 |
| `mag01`  | Gonçalves de Magalhães                   | Suspiros Poéticos e Saudades                       |        5.637 |
| `mat01`  | Gregório de Matos                        | Obra Poética                                       |       33.840 |
| `mei01*` | Cecília Meireles                         | Romanceiro                                         |        5.512 |
| `men01`  | Sá de Meneses                            | Malaca Conquistada                                 |       10.656 |
| `mit01*` | Nelci Andrado Mittmann                   | Deu Mico no Milharal                               |          164 |
| `mnc01*` | Magno Nunes Costa                        | Antônio Conselheiro                                |        2.320 |
| `muc01`  | Múcio Teixeira                           | Novos Ideais                                       |        3.132 |
| `nta01`  | Nicolau Tolentino de Almeida             | Obras Completas                                    |       10.077 |
| `nun01*` | Carlos Alberto Nunes                     | Os Brasileidas                                     |        8.504 |
| `oli01`  | Alberto de Oliveira                      | Margens de Ouro e Esmeralda                        |          187 |
| `pim01`  | Olavo Bilac e Guimarães Passos           | Pimentões                                          |        1.502 |
| `pin01`  | Xavier Pinheiro                          | Divina Comédia                                     |       14.233 |
| `rab01`  | Laurindo Rabelo                          | Poesias Completas                                  |        4.976 |
| `sil01`  | Delminda Silveira                        | Lises e Martírios                                  |        4.014 |
| `sil02`  | Delminda Silveira                        | Cancioneiro                                        |        1.197 |
| `sil03`  | Delminda Silveira                        | Passos Dolorosos                                   |          354 |
| `sil04`  | Delminda Silveira                        | Cancioneiro                                        |          552 |
| `sil05`  | Delminda Silveira                        | Poemas Dispersos                                   |        5.430 |
| `tei01*` | José C. S. Teixeira                      | Famagusta                                          |        9.280 |
| `tig01*` | Bastos Tigre                             | Saguão da Posteridade                              |        1.044 |
| `tig02*` | Bastos Tigre                             | Poemas (1903)                                      |          690 |
| `tig03*` | Bastos Tigre                             | Poemas (1904)                                      |          379 |
| `tig04*` | Bastos Tigre                             | Versos Perversos                                   |        1.947 |
| `tig05*` | Bastos Tigre                             | Moinhos de Vento                                   |        4.022 |
| `tig06*` | Bastos Tigre                             | Bolhas de Sabão                                    |        3.349 |
| `tig07*` | Bastos Tigre                             | Arlequim                                           |        1.025 |
| `tig08*` | Bastos Tigre                             | Bromilíadas                                        |        3.312 |
| `tig09*` | Bastos Tigre                             | Fonte da Carioca                                   |        3.301 |
| `tig10*` | Bastos Tigre                             | Brinquedos de Natal                                |          831 |
| `tig11*` | Bastos Tigre                             | Primeira Infância                                  |        1.214 |
| `tig12*` | Bastos Tigre                             | Poesias Humorísticas                               |        3.997 |
| `tig13*` | Bastos Tigre                             | Entardecer                                         |        2.855 |
| `tig14*` | Bastos Tigre                             | Parábolas de Cristo                                |          797 |
| `tig15*` | Bastos Tigre                             | Recitália                                          |        2.887 |
| `tig16*` | Bastos Tigre                             | Conceitos e Preceitos                              |          741 |
| `tig17*` | Bastos Tigre                             | Sátiras                                            |          925 |
| `tig18*` | Bastos Tigre                             | Eucalol                                            |          864 |
| `tig19*` | Bastos Tigre                             | Sol de Inverno                                     |        2.426 |
| `via01*` | Manoel Viana                             | Os Brasilíadas                                     |        1.120 |
|          | **Total**                                |                                                    |  **260.536** |
