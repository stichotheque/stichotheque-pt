We currently haven't published any articles on the corpus itself, but we have publications on Aoidos, our automatic scansion tool, and on results produced by it:

- Mittmann, Adiel; Pergher, Paulo Henrique; Luiz dos Santos, Alckmar.  **What Rhythmic Signature Says AboutPoetic Corpora.** In: _Quantitative Approaches to Versification_, 2019, 153–172.

- Mittmann Adiel; Luiz dos Santos, Alckmar.  **The Rhythm of Epic Verse in Portuguese from the 16th to the 21st Century.**  In: _14èmes Journées internationales d'Analyse statistique des Données Textuelles_, 2018, 514–521.

- Mittmann, Adiel; von Wangenheim, Aldo; Luiz dos Santos, Alckmar.  **Aoidos: A System for the Automatic Scansion of Poetry Written in Portuguese.** In: _Computational Linguistics and Intelligent Text Processing (CICLing) 2016. Lecture Notes in Computer Science_, 2018, vol. 9624, 611–628.
