# Nomes dos arquivos

Esquema: `cam01-1572-camões-lusíadas.xml`.

Explicação:

- `cam`: código de três letras para o autor.
- `01`: código de dois dígitos para a obra ou livro.
- `1572`: ano de publicação.
- `camões`: versão simplificada do nome do autor.
- `lusíadas`: versão simplificada do título.

Nunca usar espaço em nenhum lugar no nome do arquivo.

A primeira parte, `cam01`, identifica o arquivo de forma única.  As três primeiras letras identificam o autor de forma única.

Quando houver mais de um autor, pode-se por optar por um código que identifique os autores de forma única (por exemplo, um código especial para Olavo Bilac e Guimarães Passos) ou por algum código que identifique o corpus contido no arquivo (por exemplo, `sig` para o corpus do Siglo de Oro).

Quando o nome simplificado do autor tiver mais de um componente, separar com sublinhados: `anj01-1914-agusto_dos_anjos-obra_completa.xml`.

O ano de publicação é apenas uma referência aproximada de quando o autor escreveu a obra e não deve ser levado muito a sério.  Por exemplo: Camões publicou em 1572 a primeira edição dos *Lusíadas*, mas a edição XML pode ter sido baseada em alguma outra edição posterior.  Coloca-se, de qualquer modo, 1572, como uma indicação aproximada de quando a obra foi escrita pelo autor.  No caso de coletâneas, pode-se usar o maior ano que existir ou se souber.  Em geral, não faz muito sentido colocar um ano que vá além daquele que o autor morreu — no caso de publicações póstumas, é melhor usar o ano de morte.
